//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by Skyler Bellwood on 7/2/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var useRedText = false
    
    let motto1 = Text("Draco dormiens")
    let motto2 = Text("nunquam titillandus")
    
    var body: some View {
        
        //        Text("Hello, World!")
        //            .frame(maxWidth: .infinity, maxHeight: .infinity)
        //            .background(Color.red)
        //            .edgesIgnoringSafeArea(.all)
        
        //        Button("Hello World") {
        //            print(type(of: self.body))
        //        }
        //        .background(Color.red)
        //        .frame(width: 200, height: 200)
        
        //        Button("Hello World") {
        //
        //        }
        //        .frame(width: 200, height: 200)
        //        .background(Color.red)
        
        //        Text("Hello World")
        //        .padding()
        //            .background(Color.red)
        //        .padding()
        //            .background(Color.blue)
        //        .padding()
        //            .background(Color.green)
        //        .padding()
        //            .background(Color.yellow)
        
        //        Button("Hello World") {
        //            self.useRedText.toggle()
        //        }
        //        .foregroundColor(useRedText ? .red : .blue)
        
        //        VStack {
        //            Text("Gryffindor")
        //            Text("Hufflepuff")
        //                .blur(radius: 0)
        //            Text("Ravenclaw")
        //                .font(.title)
        //            Text("Slytherin")
        //        }
        //        .font(.largeTitle)
        //        .blur(radius: 5)
        //        VStack {
        //            motto1
        //                .foregroundColor(.red)
        //            motto2
        //                .foregroundColor(.blue)
        //        }
        
        //        VStack(spacing: 10) {
        //            Text("First")
        //                .font(.largeTitle)
        //                .padding()
        //                .foregroundColor(.white)
        //                .background(Color.blue)
        //                .clipShape(Capsule())
        //
        //            Text("Second")
        //                .font(.largeTitle)
        //                .padding()
        //                .foregroundColor(.white)
        //                .background(Color.blue)
        //                .clipShape(Capsule())
        //        }
        
        //        VStack(spacing: 10) {
        //            CapsuleText(text: "First")
        //                .foregroundColor(.white)
        //            CapsuleText(text: "Second")
        //                .foregroundColor(.yellow)
        //        }
        
        //        VStack(spacing: 10) {
        //            CapsuleText(text: "First")
        //                .foregroundColor(.white)
        //            CapsuleText(text: "Second")
        //                .foregroundColor(.yellow)
        //            Text("Hello World")
        //            .modifier(Title())
        //        }
        
        //        Text("Hello World")
        //            .titleStyle()
        
//        Color.blue
//            .frame(width: 300, height: 200)
//            .watermarked(with: "Hacking with Swift")
//
//        GridStack(rows: 9, columns: 9) { row, col in
//            Text("\(row)\(col)")
//            .padding(5)
//        }
        
//        GridStack(rows: 4, columns: 4) { row, col in
//            HStack {
//                Image(systemName: "\(row * 4 + col).circle")
//                Text("\(row) \(col)")
//            }
//        }
        
        VStack {
            Text("Challenge Title")
                .largeBlueTitleStyle()
            GridStack(rows: 4, columns: 4) { row, col in
                Image(systemName: "\(row * 4 + col).circle")
                Text("R\(row) C\(col)")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CapsuleText: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.largeTitle)
            .padding()
            //            .foregroundColor(.white)
            .background(Color.blue)
            .clipShape(Capsule())
    }
}

struct Title: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.white)
            .padding()
            .background(Color.blue)
            .clipShape(RoundedRectangle(cornerRadius: 10))
    }
}

struct Watermark: ViewModifier {
    var text: String
    
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
            Text(text)
                .font(.caption)
                .foregroundColor(.white)
                .padding(5)
                .background(Color.black)
        }
    }
}

struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content
    
    init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
        self.rows = rows
        self.columns = columns
        self.content = content
    }
    
    var body: some View {
        VStack(spacing: 10) {
            ForEach(0 ..< rows, id: \.self) { row in
                HStack(spacing: 10) {
                    ForEach(0 ..< self.columns, id: \.self) { column in
                        self.content(row, column)
                    }
                }
            }
        }
    }
}

struct LargeBlueTitle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.blue)
    }
}

extension View {
    func watermarked(with text: String) -> some View {
        self.modifier(Watermark(text: text))
    }
}

extension View {
    func titleStyle() -> some View {
        self.modifier(Title())
    }
}

extension View {
    func largeBlueTitleStyle() -> some View {
        self.modifier(LargeBlueTitle())
    }
}
